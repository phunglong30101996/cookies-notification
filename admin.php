<?php 

require 'vendor/autoload.php';

use sandeepshetty\shopify_api;

require 'conn-shopify.php';

//session_start();

if (isset($_GET['shop'])) {
    $shop = $_GET['shop'];
} else {
    ?>
    <head>
        <script src="admin/lib/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="admin/lib/bootstrap.min.css">
        <script type="text/javascript" src="admin/lib/bootstrap.min.js"></script>
		<script src="admin/lib/app.js"></script>
    </head>
    <div class="container">
        <h1 class="page-heading">Input your shop name to continue: </h1>
        <form class="form-inline">
            <input type="text" style="width: 500px" class="inputShop form-control">
            <button class="btn btn-primary submitShop" type="submit">Continue</button>
        </form>
    </div>
    <script>
        $('.submitShop').click(function (e) {
            e.preventDefault();
            window.location = 'https://' + $('.inputShop').val() + '/admin/api/auth?api_key=<?php echo $apiKey; ?>';
        });
    </script>
    <?php
    die();
}
$shop_data = $db->query("select * from tbl_usersettings where store_name = '" . $shop . "' and app_id = $appId");
$shop_data = $shop_data->fetch_object();
$installedDate = $shop_data->installed_date;
$confirmation_url = $shop_data->confirmation_url;
$clientStatus = $shop_data->status;

$date1 = new DateTime($installedDate);
$date2 = new DateTime("now");
$interval = date_diff($date1, $date2);
$diff = (int) $interval->format('%R%a');

$select_settings = $db->query("SELECT * FROM tbl_appsettings WHERE id = $appId");
$app_settings = $select_settings->fetch_object();
$shop_data = $db->query("select * from tbl_usersettings where store_name = '" . $shop . "' and app_id = $appId");
$shop_data = $shop_data->fetch_object();
$shopify = shopify_api\client(
        $shop, $shop_data->access_token, $app_settings->api_key, $app_settings->shared_secret
);
$shopInfo = $shopify("GET", "/admin/shop.json");
?>
<!DOCTYPE html>
<head>
    <title>EU GDPR Cookies Notification Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300|Roboto:300,400,500,700,400italic|Material+Icons&font-display=swap">
    <link rel="stylesheet" href="admin/lib/vue-multiselect.min.css">
    <link rel="stylesheet" href="admin/lib/vue-material.min.css">
    <link rel="stylesheet" href="admin/lib/default.css">
    <link rel="stylesheet" href="https://unpkg.com/element-ui@2.4.11/lib/theme-chalk/index.css">
    <link rel="stylesheet" href="admin/lib/element-ui/element-ui.css">
    <link rel="stylesheet" type="text/css" href="admin/styles/appStyles.css?v=<?php echo time() ?>">
    <script src="admin/lib/jquery.min.js"></script>
</head>
<body id="cookiesNotificationBody">
    <span id="shop" style="display: none" class="shopName"><?php echo $shop; ?></span>
    <!--<p class="noteWrap"><span style="color:red"><strong>Attention:</strong></span> We had some improvements to save cookie when submit, so customer maybe must submit again. Sorry for this inconvenience!<a href="#" class="closeNote">Close X</a></p>-->
    <div layout-padding style="width: 100%">
        <div id="cookiesNotificationApp">
            <div class="page-container">
                <settings></settings>
            </div>
        </div>
        <div class="app-footer">
            <div class="footer-header">Some other sweet <strong>Omega</strong> apps you might like! <a target="_blank" href="https://apps.shopify.com/partners/developer-30c7ea676d888492">(View all app)</a></div>
            <div class="omg-more-app">
                <div>
                    <p><a href="https://apps.shopify.com/quantity-price-breaks-limit-purchase" target="_blank"><img alt="Quantity Price Breaks by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/5143/splash.png?1452220345"></a></p>
                </div>
                <div>
                    <p><a href="https://apps.shopify.com/facebook-reviews-1" target="_blank"><img alt="Facebook Reviews by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/13331/splash.png?1499916138"></a></p>
                </div>
                <div>
                    <p><a href="https://apps.shopify.com/order-tagger-by-omega" target="_blank"><img alt="Order Tagger by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/17108/splash.png?1510565540"></a></p>
                </div>
            </div>
        </div>
    </div>
    <?php include 'facebook-chat.html'; ?>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script type="text/javascript" src="admin/lib/bootstrap.min.js"></script>
    <script type="text/javascript">
        ShopifyApp.init({
          apiKey: '<?php echo $apiKey; ?>',
          shopOrigin: 'https://<?php echo $shop; ?>',

        });
        ShopifyApp.ready(function(){
            ShopifyApp.Bar.initialize({
                title: 'Edit Settings'
            });
        });	
    </script>
    <script>
        $(".closeNote").click(function (e) {
            e.preventDefault();
            $(".noteWrap").hide();
        });
        $(".refreshCharge").click(function (e) {
            e.preventDefault();
            $.get("recharge.php?shop=<?php echo $shop; ?>", function (result) {
                location.href = result;
            });
        });
        window.shop = "<?php echo $shop; ?>";
        window.rootLink = "<?php echo $rootLink; ?>";
    </script>
	
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126587266-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-126587266-1');
		</script>
		<?php include 'google_remarketing_tag.txt'; ?>	
	
    <script type="text/javascript" src="admin/lib/vue.js"></script>
    <script type="text/javascript" src="admin/lib/httpVueLoader.js"></script>
    <script type="text/javascript" src="admin/vuejs/vue-resource.min.js"></script>
    <script type="text/javascript" src="admin/lib/vue-material.min.js"></script>
    <script type="text/javascript" src="admin/lib/vue-multiselect.min.js"></script>
    <script type="text/javascript" src="admin/lib/element-ui/index.js"></script>
    <script type="text/javascript" src="admin/lib/element-ui/en.js"></script>
    <script type="text/javascript" src="admin/scripts/main.js?v=?v=<?php echo time() ?>"></script>
    <script>
        ELEMENT.locale(ELEMENT.lang.en)
    </script>
</body>