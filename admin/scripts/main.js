const rootLink = window.rootLink
Vue.use(VueMaterial.default);
new Vue({
    el: '#cookiesNotificationApp',
    data: function () {
        return {

        }
    },
    created: function () {

    },
    methods: {

    },
    components: {
        settings: httpVueLoader(
            `${rootLink}/admin/components/settings.vue?v=` + new Date().getTime()
        ),
    }
})
