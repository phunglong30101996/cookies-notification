<?php 

require 'vendor/autoload.php';

use sandeepshetty\shopify_api;

require 'conn-shopify.php';

//session_start();

if (isset($_GET['shop'])) {
    $shop = $_GET['shop'];
} else {
    ?>
    <head>
        <script src="admin/lib/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="admin/vuejs/bootstrap.min.css">
        <script type="text/javascript" src="admin/vuejs/bootstrap.min.js"></script>
		<script src="admin/lib/app.js"></script>
    </head>
    <div class="container">
        <h1 class="page-heading">Input your shop name to continue: </h1>
        <form class="form-inline">
            <input type="text" style="width: 500px" class="inputShop form-control">
            <button class="btn btn-primary submitShop" type="submit">Continue</button>
        </form>
    </div>
    <script>
        $('.submitShop').click(function (e) {
            e.preventDefault();
            window.location = 'https://' + $('.inputShop').val() + '/admin/api/auth?api_key=<?php echo $apiKey; ?>';
        });
    </script>
    <?php
    die();
}
$shop_data = $db->query("select * from tbl_usersettings where store_name = '" . $shop . "' and app_id = $appId");
$shop_data = $shop_data->fetch_object();
$installedDate = $shop_data->installed_date;
$confirmation_url = $shop_data->confirmation_url;
$clientStatus = $shop_data->status;

$date1 = new DateTime($installedDate);
$date2 = new DateTime("now");
$interval = date_diff($date1, $date2);
$diff = (int) $interval->format('%R%a');

$select_settings = $db->query("SELECT * FROM tbl_appsettings WHERE id = $appId");
$app_settings = $select_settings->fetch_object();
$shop_data = $db->query("select * from tbl_usersettings where store_name = '" . $shop . "' and app_id = $appId");
$shop_data = $shop_data->fetch_object();
$shopify = shopify_api\client(
        $shop, $shop_data->access_token, $app_settings->api_key, $app_settings->shared_secret
);
$shopInfo = $shopify("GET", "/admin/shop.json");
if($shop == 'ha-testapp.myshopify.com'){
?>
<head>
    <title>EU Cookies Notification Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="admin/vuejs/vue-toasted.min.css">
    <link rel="stylesheet" href="admin/vuejs/vue-multiselect.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300|Roboto:300,400,500,700,400italic|Material+Icons">
    <link rel="stylesheet" href="admin/vuejs/vue-material.min.css">
    <link rel="stylesheet" href="admin/vuejs/default.css">
    <link rel="stylesheet" type="text/css" href="admin/styles/appStyles.css?v=1">
    <script src="admin/vuejs/jquery.min.js"></script>
</head>
<body id="cookiesNotificationBody">
    <span id="shop" style="display: none" class="shopName"><?php echo $shop; ?></span>
    <?php require 'admin/review/star.php'; ?>
    <div layout-padding style="width: 100%">
        <toaster-container></toaster-container>
        <div id="cookiesNotificationApp">
            <div class="page-container">
                <settings :settings='settings'></settings>
            </div>
        </div>
        <div class="app-footer">
            <div class="footer-header">Some other sweet <strong>Omega</strong> apps you might like! <a target="_blank" href="https://apps.shopify.com/partners/developer-30c7ea676d888492">(View all app)</a></div>
            <div class="omg-more-app">
                <div>
                    <p><a href="https://apps.shopify.com/quantity-price-breaks-limit-purchase" target="_blank"><img alt="Quantity Price Breaks by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/5143/splash.png?1452220345"></a></p>
                </div>
                <div>
                    <p><a href="https://apps.shopify.com/facebook-reviews-1" target="_blank"><img alt="Facebook Reviews by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/13331/splash.png?1499916138"></a></p>
                </div>
                <div>
                    <p><a href="https://apps.shopify.com/order-tagger-by-omega" target="_blank"><img alt="Order Tagger by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/17108/splash.png?1510565540"></a></p>
                </div>
            </div>
        </div>
    </div>
    <?php include 'facebook-chat.html'; ?>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script type="text/javascript" src="admin/vuejs/bootstrap.min.js"></script>
    <script type="text/javascript">
        ShopifyApp.init({
          apiKey: '<?php echo $apiKey; ?>',
          shopOrigin: 'https://<?php echo $shop; ?>',

        });
        ShopifyApp.ready(function(){
            ShopifyApp.Bar.initialize({
                title: 'Edit Settings'
            });
        });	
    </script>
    <script>
        $(".closeNote").click(function (e) {
            e.preventDefault();
            $(".noteWrap").hide();
        });
        $(".refreshCharge").click(function (e) {
            e.preventDefault();
            $.get("recharge.php?shop=<?php echo $shop; ?>", function (result) {
                location.href = result;
            });
        });
        window.shop = "<?php echo $shop; ?>";
        window.rootLink = "<?php echo $rootLink; ?>";
    </script>
    <script type="text/javascript" src="admin/vuejs/vue.js"></script>
    <script type="text/javascript" src="admin/vuejs/httpVueLoader.js"></script>
    <script type="text/javascript" src="admin/vuejs/vue-resource.min.js"></script>
    <script type="text/javascript" src="admin/vuejs/vue-material.min.js"></script>
    <script type="text/javascript" src="admin/vuejs/vue-multiselect.min.js"></script>
    <script type="text/javascript" src="admin/vuejs/vue-toasted.min.js"></script>
    <script type="text/javascript" src="admin/scripts/main.js?v=1"></script>
</body>
<?php } else { ?>
<head>
    <title>EU Cookies Notification Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="admin/lib/bootstrap.min.css">
    <link rel="stylesheet" href="admin/lib/vue-toasted.min.css">
    <link rel="stylesheet" href="admin/lib/vue-multiselect.min.css">
    <link rel="stylesheet" href="admin/lib/bootstrap-vue.css">
    <link rel="stylesheet" type="text/css" href="admin/styles/styles.css?v=1">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="admin/lib/jquery.min.js"></script>
    <script type="text/javascript" src="admin/lib/bootstrap.min.js"></script>
        <script type="text/javascript">
        ShopifyApp.init({
          apiKey: '<?php echo $apiKey; ?>',
          shopOrigin: 'https://<?php echo $shop; ?>',

        });
        ShopifyApp.ready(function(){
                ShopifyApp.Bar.initialize({});
        });	
  </script>
</head>
<body id="cookiesNotificationBody">
    <script type="text/javascript">
        ShopifyApp.ready(function(){
            ShopifyApp.Bar.initialize({
                title: 'Edit Settings'
            });
	});
    </script>
    <span id="shop" style="display: none" class="shopName"><?php echo $shop; ?></span>
<!--	<p class="review-message">Love the app? <a href="https://apps.shopify.com/eu-cookies-notification?reveal_new_review=true" target="_blank">Leave us a review</a> (it helps a lot!)<a href="#" class="closeNote">X</a></p>	-->
	<!--<p class="noteWrap">Love the app? <a href="https://apps.shopify.com/eu-cookies-notification?reveal_new_review=true" target="_blank">Leave us a review</a> (it helps a lot!)<a href="#" class="closeNote">Close X</a></p>-->
	<?php require 'admin/review/star.php'; ?>
        <div layout-padding style="width: 100%">
            <toaster-container></toaster-container>
            <div id="cookiesNotificationApp" class="col-xs-12" style="padding:0">
                <div class="col-md-5 col-sm-6">
                    <h3 class="page-heading">General Settings</h3>
                    <div class=" col-xs-12 app-content">
                        <div class="form-group col-xs-12">
                            <label>Enable popup on your store</label>
                            <div>
                                <input v-model="settings.app_enable" type="checkbox" name="">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Message</label>
                            <div>
                                <input v-model="settings.message" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Submit button text</label>
                            <div>
                                <input v-model="settings.submit_text" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>More information link text</label>
                            <div>
                                <input v-model="settings.info_text" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Your Privacy Policy URL</label>
                            <div>
                                <input v-model="settings.privacy_link" class="form-control" type="text" placeholder="https://<?php echo $shop ?>/privacy-policy-page">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Popup's layout</label>
                            <div>
                                <select v-model="settings.popup_layout" class="form-control">
                                    <option value="1">Full width</option>
                                    <option value="2">Corner popup</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12" v-if="settings.popup_layout == 1">
                            <label>Position</label>
                            <div>
                                <select v-model="settings.fullwidth_position" class="form-control">
                                    <option value="1">Top</option>
                                    <option value="2">Bottom</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12" v-if="settings.popup_layout == 2">
                            <label>Position</label>
                            <div class="col-xs-12" style="padding: 0;">
                                <div class="col-sm-3">
                                    <label class="radio">
                                        <img src="admin/images/position_4.jpg" width="100%">
                                        <div class="btn_position">
                                            <input type="radio" name="style"  v-model="settings.corner_position" value="1">
                                            <span class="outer"><span class="inner"></span></span>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <label class="radio">
                                        <img src="admin/images/position_1.jpg"  width="100%" >
                                        <div class="btn_position">
                                            <input type="radio" name="style" v-model="settings.corner_position" value="2">
                                            <span class="outer"><span class="inner"></span></span>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <label class="radio">
                                        <img src="admin/images/position_3.jpg"  width="100%" >
                                        <div class="btn_position">
                                            <input type="radio" name="style" v-model="settings.corner_position" value="3">
                                            <span class="outer"><span class="inner"></span></span>
                                        </div> 
                                    </label>
                                </div>
                                <div class="col-sm-3"> 
                                    <label class="radio">
                                        <img src="admin/images/position_2.jpg" width="100%">
                                        <div class="btn_position">
                                            <input type="radio" name="style" v-model="settings.corner_position" value="4">
                                            <span class="outer"><span class="inner"></span></span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Enable popup for all countries</label>
                            <div>
                                <input type="radio" id="show_all1" value="1" v-model="settings.show_all">
                                <label for="show_all1">&nbsp;Yes</label>&nbsp;&nbsp;
                                <input type="radio" id="show_all0" value="0" v-model="settings.show_all">
                                <label for="show_all0">&nbsp;No</label>
                            </div>
                        </div>
                        <div class="form-group col-xs-12" v-if="settings.show_all == 0">
                            <label>Enable popup just for all EU countries</label>
                            <div>
                                <input type="radio" id="show_all_eu1" value="1" v-model="settings.show_all_eu">
                                <label for="show_all_eu1">&nbsp;Yes</label>&nbsp;&nbsp;
                                <input type="radio" id="show_all_eu0" value="0" v-model="settings.show_all_eu">
                                <label for="show_all_eu0">&nbsp;No</label>
                            </div>
                        </div>
                        <div class="form-group col-xs-12" v-if="settings.show_all == 0 && settings.show_all_eu == 0">
                            <label>or for specified EU countries</label>
                            <div>
                                <multiselect 
                                    v-model="settings.eu_countries" 
                                    :options="eu_countries"
                                    :multiple="true"
                                    :close-on-select="true"
                                    label="title"
                                    track-by="title"
                                    >
                                </multiselect>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Remember submit for (days)</label>
                            <div>
                                <input v-model="settings.cache_time" class="form-control" type="number" min="1" max="60">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Popup background color</label>
                            <div>
                                <input type="color" class="inputColor form-control" v-model="settings.popup_bgcolor">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Popup text color</label>
                            <div>
                                <input type="color" class="inputColor form-control" v-model="settings.popup_textcolor">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>More information link text color</label>
                            <div>
                                <input type="color" class="inputColor form-control" v-model="settings.more_textcolor">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Submit background color</label>
                            <div>
                                <input type="color" class="inputColor form-control" v-model="settings.submit_bgcolor">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Submit text and border color</label>
                            <div>
                                <input type="color" class="inputColor form-control" v-model="settings.submit_textcolor">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Custom CSS</label>
                            <div>
                                <textarea class="form-control" v-model="settings.custom_css" name=""></textarea>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <button class="btn btn-primary" v-on:click="saveSettings">Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-6">
                    <h3 class="page-heading">Layout Preview</h3>
                    <div style="overflow: hidden;padding:5px; margin:10px 0px 30px 0px; border-radius:2px; font-weight:bold; font-size:0.9em; border:1px solid #CCC; background-color:#F5f5f5;">Attention!
                                This is a preview only. The visuals might differ after applying settings due to extra styling rules that your Theme might have or the available space. </div>
                    <img style="width: 100%" src="assets/images/gd.png">
                    <div class="preview-app otCookiesNotification" v-if="settings.popup_layout == 1">
                        <link rel='stylesheet' type='text/css' href='admin/styles/demoApp.css?v=1' />
                        <div id='cookies-wrapper' v-if="settings.fullwidth_position == 1" v-bind:style="{ top: 0, color: settings.popup_textcolor, background: settings.popup_bgcolor}">
                            <div id='cookies-message'>{{ settings.message }}&nbsp;<a id='cookies-more-info' :href="settings.privacy_link" v-bind:style="{ color: settings.more_textcolor }">{{ settings.info_text }}</a></div>
                            <div id='cookies-submit'><a v-bind:style="{ color: settings.submit_textcolor, background: settings.submit_bgcolor, borderColor: settings.submit_textcolor }">{{ settings.submit_text }}</a></div>
                        </div>
                        <div id='cookies-wrapper' v-if="settings.fullwidth_position == 2" v-bind:style="{ bottom: 0, color: settings.popup_textcolor, background: settings.popup_bgcolor}">
                            <div id='cookies-message'>{{ settings.message }}&nbsp;<a id='cookies-more-info' :href="settings.privacy_link" v-bind:style="{ color: settings.more_textcolor }">{{ settings.info_text }}</a></div>
                            <div id='cookies-submit'><a v-bind:style="{ color: settings.submit_textcolor, background: settings.submit_bgcolor, borderColor: settings.submit_textcolor }">{{ settings.submit_text }}</a></div>
                        </div>
                    </div>
                    <div class="preview-app otCookiesNotification" v-if="settings.popup_layout == 2">
                        <link rel='stylesheet' type='text/css' href='admin/styles/demoApp-corner.css?v=1' />
                        <div id='cookies-wrapper' v-if="settings.corner_position == 1" v-bind:style="{ top: 0, left: 0, color: settings.popup_textcolor, background: settings.popup_bgcolor}">
                            <div id='cookies-message'>{{ settings.message }}</div>
                            <div id='cookies-submit'><a v-bind:style="{ color: settings.submit_textcolor, background: settings.submit_bgcolor, borderColor: settings.submit_textcolor }">{{ settings.submit_text }}</a></div>
                        </div>
                        <div id='cookies-wrapper' v-if="settings.corner_position == 2" v-bind:style="{ bottom: 0, left: 0, color: settings.popup_textcolor, background: settings.popup_bgcolor}">
                            <div id='cookies-message'>{{ settings.message }}</div>
                            <div id='cookies-submit'><a v-bind:style="{ color: settings.submit_textcolor, background: settings.submit_bgcolor, borderColor: settings.submit_textcolor }">{{ settings.submit_text }}</a></div>
                        </div>
                        <div id='cookies-wrapper' v-if="settings.corner_position == 3" v-bind:style="{ top: 0, right: 0, color: settings.popup_textcolor, background: settings.popup_bgcolor}">
                            <div id='cookies-message'>{{ settings.message }}</div>
                            <div id='cookies-submit'><a v-bind:style="{ color: settings.submit_textcolor, background: settings.submit_bgcolor, borderColor: settings.submit_textcolor }">{{ settings.submit_text }}</a></div>
                        </div>
                        <div id='cookies-wrapper' v-if="settings.corner_position == 4" v-bind:style="{ bottom: 0, right: 0, color: settings.popup_textcolor, background: settings.popup_bgcolor}">
                            <div id='cookies-message'>{{ settings.message }}</div>
                            <div id='cookies-submit'><a v-bind:style="{ color: settings.submit_textcolor, background: settings.submit_bgcolor, borderColor: settings.submit_textcolor }">{{ settings.submit_text }}</a></div>
                            <div id='cookies-privacy'><a id='cookies-more-info' :href="settings.privacy_link" v-bind:style="{ color: settings.more_textcolor }">{{ settings.info_text }}</a></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="app-footer col-xs-12">
                <div class="footer-header">Some other sweet <strong>Omega</strong> apps you might like! <a target="_blank" href="https://apps.shopify.com/partners/developer-30c7ea676d888492">(View all app)</a></div>
                <div class="omg-more-app">
                    <div>
                        <p><a href="https://apps.shopify.com/quantity-price-breaks-limit-purchase" target="_blank"><img alt="Quantity Price Breaks by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/5143/splash.png?1452220345"></a></p>
                    </div>
                    <div>
                        <p><a href="https://apps.shopify.com/facebook-reviews-1" target="_blank"><img alt="Facebook Reviews by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/13331/splash.png?1499916138"></a></p>
                    </div>
                    <div>
                        <p><a href="https://apps.shopify.com/order-tagger-by-omega" target="_blank"><img alt="Order Tagger by Omega" src="https://s3.amazonaws.com/shopify-app-store/shopify_applications/small_banners/17108/splash.png?1510565540"></a></p>
                    </div>
                </div>
            </div>		
	</div>
	
	<?php include 'facebook-chat.html'; ?>
	<script src="admin/lib/jquery.min.js"></script>
    <script>
        $(".closeNote").click(function(e){
            e.preventDefault();
            $(".noteWrap").hide();
        });
        $(".refreshCharge").click(function(e){
            e.preventDefault();
            $.get("recharge.php?shop=<?php echo $shop; ?>", function(result){
                location.href = result;
            });
        });

		
    </script>	
    <script type="text/javascript" src="admin/lib/vue.js"></script>
    <script type="text/javascript" src="admin/lib/bootstrap-vue.js"></script>
    <script type="text/javascript" src="admin/lib/toasted.min.js"></script>
    <script type="text/javascript" src="admin/lib/vue-multiselect.min.js"></script>
    <script type="text/javascript" src="admin/lib/vue-toasted.min.js"></script>
    <script type="text/javascript" src="admin/scripts/settings.js?v=1"></script>
</body>
<?php } ?>

