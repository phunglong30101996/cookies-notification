let omgcookies_shopName = Shopify.shop;
let omgcookies_storeSettings;
let omgcookies_nameSetNotificationCookie = 'cookiesNotification';
var cookies = [];
var data = {}
data.name = "";
data.action = "getCookies";
function omgcookies_getJsonFile() {
    // setInterval(function () {
    //     omgrfq_checkOnChangeVariant();
    // }, 1000);
    var d = new Date(),
        v = d.getTime();
    console.log(document.cookie)
    $.getJSON(`${rootlinkCookiesNotification}/cache/${omgcookies_shopName}/data.json?v=${v}`, (data) => {
        omgcookies_storeSettings = data.settings;
        if (omgcookies_storeSettings && omgcookies_storeSettings.app_enable == 1) {
            $("body").append("<div class='otCookiesNotification'></div>");
            if (omgcookies_storeSettings.popup_layout == 1) {
                $('head').append(`
                    <link href='${rootlinkCookiesNotification}/assets/css/cookies-notification.css?v=${v}' rel='stylesheet' type='text/css'>
                `);
            } else {
                $('head').append(`
                    <link href='${rootlinkCookiesNotification}/assets/css/cookies-notification-corner.css?v=${v}' rel='stylesheet' type='text/css'>
                `);
            }
            var getCookiesNotification = omgcookies_getCookie(omgcookies_nameSetNotificationCookie);
            if (getCookiesNotification == null || getCookiesNotification == '') {
                omgcookies_getShopLocation(omgcookies_getCookiesNotification);
            }
        }
    });
}
function omgcookies_getCookies(data) {
    $.get(rootlinkCookiesNotification + '/cookies-notification.php', data, function(result) {
          if (typeof result == "string") { 
            result = JSON.parse(result);
          }
          if(result.position == 'Before'){
            $(".step__footer").before(result.html); 
          }else{
            $(".step__footer").after(result.html); 
          }
    });
 }
function omgcookies_getShopLocation(callback) {
    if (omgcookies_storeSettings.show_all == 1) {
        if (typeof callback == "function") {
            callback();
        }
    } else {
        if (omgcookies_storeSettings.eu_countries != null && omgcookies_storeSettings.eu_countries != '') {
            var eu_countries = JSON.parse(omgcookies_storeSettings.eu_countries)
        } else {
            var eu_countries = [];
        }
        $.get(rootlinkCookiesNotification + '/cookies-notification.php', {
            action: 'getShopLocation',
            shop: omgcookies_shopName,
            all_eu: omgcookies_storeSettings.show_all_eu,
            eu_countries: eu_countries
        }, function (result) {
            if (result == 1 && typeof callback == "function") {
                callback();
            }
        });
    }
}
function omgcookies_getCookiesNotification() {
    var privacy = (omgcookies_storeSettings.privacy_link != '') ? `<a target="_blank" id="cookies-more-info" href="${omgcookies_storeSettings.privacy_link}">${omgcookies_storeSettings.info_text}</a>` : omgcookies_storeSettings.info_text;
    if (omgcookies_storeSettings.popup_layout == 1) {
        var layoutCss = "";
        if (omgcookies_storeSettings.fullwidth_position == 1) {
            layoutCss += `.otCookiesNotification #cookies-wrapper{top:0;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
            layoutCss +=`.otCookiesNotification #container-detail{top:60px;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
        } else if (omgcookies_storeSettings.fullwidth_position == 2) {
            layoutCss += `.otCookiesNotification #cookies-wrapper{bottom:0;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
            layoutCss += `.otCookiesNotification #container-detail{bottom:60px;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
        }
        $("body").append(`
            <style>
                ${layoutCss}
                #cookies-more-info{color:${omgcookies_storeSettings.more_textcolor};}
                #cookies-submit > a{color:${omgcookies_storeSettings.submit_textcolor};background:${omgcookies_storeSettings.submit_bgcolor};border-color:${omgcookies_storeSettings.submit_textcolor};}
                ${omgcookies_storeSettings.custom_css}
            </style>`);
        $('.otCookiesNotification').append(`
        <div id='cookies-wrapper'>
                <div id='cookies-message'>${omgcookies_storeSettings.message}&nbsp;${privacy}</div>
                <div id='cookies-submit'><a class='ot-cookie' onclick='omgcookies_cookiesSubmit()'>${omgcookies_storeSettings.submit_text}</a></div>
                <div id="cookies-detail" style="align-items: center; margin-left: 10px;">
                <a
                  id="cookies-more-info"
                  onclick='ShowDetail()'
                 
                >Details</a>
              </div>
              <div id="cookies-detail-hidden" style="align-items: center; margin-left: 10px;">
              <a
                id="cookies-more-info"
                onclick='HiddenDetail()'
                
              >Details</a>
        </div>
        <div id="container-detail" class="container-detail" style="display: none;">
        <div>
            <div class="omega-row">
            <a href="javascript:void(0)" onclick="openType(event, 'Necessary');">
                <div class="omega-third tabLink omega-bottomBar omega-hover-light-grey omega-padding">Necessary</div>
            </a>
            <a href="javascript:void(0)" onclick="openType(event, 'Preferences');">
                <div class="omega-third tabLink omega-bottomBar omega-hover-light-grey omega-padding">Preferences</div>
            </a>
            <a href="javascript:void(0)" onclick="openType(event, 'Statistics');">
                <div class="omega-third tabLink omega-bottomBar omega-hover-light-grey omega-padding">Statistics</div>
            </a>
            <a href="javascript:void(0)" onclick="openType(event, 'Marketing');">
                <div class="omega-third tabLink omega-bottomBar omega-hover-light-grey omega-padding">Marketing</div>
            </a>
            <a href="javascript:void(0)" onclick="openType(event, 'Unclassified');">
                <div class="omega-third tabLink omega-bottomBar omega-hover-light-grey omega-padding">Unclassified</div>
            </a>
            <a href="javascript:void(0)" onclick="openType(event, 'About');">
                <div class="omega-third tabLink omega-bottomBar omega-hover-light-grey omega-padding">About Cookies</div>
            </a>
        </div>
           <div id="Necessary" class="omega-container typeCity" style="display:none">
           <table>
           <tr>
           <th>Name</th>
           <th>Provider</th>
           <th>Purpose</th>
           <th>Expiry</th>
           <th>Type</th>
           </tr>
           <tr>
           <td>__RequestVerificationToken</td>
           <td>CookieBot</td>
           <td>Helps prevent Cross-Site Request Forgery (CSRF) attacks.</td>
           <td>Session</td>
           <td>HTTP</td>
           </tr>
           <tr>
           <td>ASPXAUTH</td>
           <td>CookieBot</td>
           <td>Identifies the user and allows authentication to the server.</td>
           <td>Session</td>
           <td>HTTPs</td>
           </tr>
           </table>
          </div>
      
          <div id="Preferences" class="omega-container typeCity" style="display:none">
          <table>
           <tr>
           <th>Name</th>
           <th>Provider</th>
           <th>Purpose</th>
           <th>Expiry</th>
           <th>Type</th>
           </tr>
           <tr>
           <td>__Token</td>
           <td>Cookie</td>
           <td>Helps prevent.</td>
           <td>Session</td>
           <td>HTTP</td>
           </tr>
           <tr>
           <td>ASPXAUTH</td>
           <td>CookieBot</td>
           <td>Identifies the user and allows authentication to the server.</td>
           <td>Session</td>
           <td>HTTPs</td>
           </tr>
           </table>
          </div>
      
          <div id="Statistics" class="omega-container typeCity" style="display:none">
          <table>
           <tr>
           <th>Name</th>
           <th>Provider</th>
           <th>Purpose</th>
           <th>Expiry</th>
           <th>Type</th>
           </tr>
           <tr>
           <td>__VerificationToken</td>
           <td>CookieBot</td>
           <td>Helps prevent Cross-Site attacks.</td>
           <td>Session</td>
           <td>HTTP</td>
           </tr>
           <tr>
           <td>ASPX</td>
           <td>CookieBot</td>
           <td>Identifies to the server.</td>
           <td>Session</td>
           <td>HTTPs</td>
           </tr>
           </table>
          </div>
          <div id="Marketing" class="omega-container typeCity" style="display:none">
          <table>
           <tr>
           <th>Name</th>
           <th>Provider</th>
           <th>Purpose</th>
           <th>Expiry</th>
           <th>Type</th>
           </tr>
           <tr>
           <td>__RequestVerificationToken</td>
           <td>CookieBot</td>
           <td>Helps prevent Cross-Site Request Forgery (CSRF) attacks.</td>
           <td>Session</td>
           <td>HTTP</td>
           </tr>
           <tr>
           <td>ASPXAUTH</td>
           <td>CookieBot</td>
           <td>Identifies the user and allows authentication to the server.</td>
           <td>Session</td>
           <td>HTTPs</td>
           </tr>
           </table>
          </div>
    
          <div id="Unclassified" class="omega-container typeCity" style="display:none">
          <table>
           <tr>
           <th>Name</th>
           <th>Provider</th>
           <th>Purpose</th>
           <th>Expiry</th>
           <th>Type</th>
           </tr>
           <tr>
           <td>__Request</td>
           <td>CookieBot</td>
           <td>Helps prevent Cross-Site Request Forgery (CSRF) attacks.</td>
           <td>Session</td>
           <td>HTTP</td>
           </tr>
           <tr>
           <td>Asp.net</td>
           <td>CookieBot</td>
           <td>Identifies the user and allows authentication to the server.</td>
           <td>Session</td>
           <td>HTTPs</td>
           </tr>
           </table>
          </div>
    
          <div id="About" class="omega-container typeCity" style="display:none">
          <p>Cookies are small text files that can be used by websites to make a user's experience more efficient.
          The law states that we can store cookies on your device if they are strictly necessary for the operation of this site. For all other types of cookies we need your permission.
          This site uses different types of cookies. Some cookies are placed by third party services that appear on our pages.
          You can at any time change or withdraw your consent from the Cookie Declaration on our website.
          Learn more about who we are, how you can contact us and how we process personal data in our Privacy Policy.
          Your consent applies to the following domains: www.cookiebot.com, manage.cookiebot.com, support.cookiebot.com, www.cybot.com</p>
          </div>
        </div>
            <div id="cookies" >
                <a
                  id="cookies-more-info"
                    href="https://apps.omegatheme.com/"
                >Power By Omega</a>
            </div>
        </div>
        `);
    } else {
        switch (omgcookies_storeSettings.corner_position) {
            case 2:
                var layoutCss = `.otCookiesNotification #cookies-wrapper{bottom:1em;left:1em;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
                break;
            case 3:
                var layoutCss = `.otCookiesNotification #cookies-wrapper{top:1em;right:1em;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
                break;
            case 4:
                var layoutCss = `.otCookiesNotification #cookies-wrapper{bottom:1em;right:1em;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
                break;
            default:
                var layoutCss = `.otCookiesNotification #cookies-wrapper{top:1em;left:1em;background:${omgcookies_storeSettings.popup_bgcolor};color:${omgcookies_storeSettings.popup_textcolor};}`;
                break;
        }
        $("body").append(`
            <style>
                ${layoutCss}
                #cookies-more-info{color:${omgcookies_storeSettings.more_textcolor};}
                #cookies-submit > a{color:${omgcookies_storeSettings.submit_textcolor};background:${omgcookies_storeSettings.submit_bgcolor};border-color:${omgcookies_storeSettings.submit_textcolor};}
                #cookies-privacy{color:${omgcookies_storeSettings.submit_textcolor};}
                #cookies-privacy:hover{border-color:${omgcookies_storeSettings.submit_textcolor};}
                ${omgcookies_storeSettings.custom_css}
            </style>`);
        $('.otCookiesNotification').append(`
            <div id='cookies-wrapper'>
                <div id='cookies-message'>${omgcookies_storeSettings.message}</div>
                <div id='cookies-submit'><a class='ot-cookie' onclick='omgcookies_cookiesSubmit()'>${omgcookies_storeSettings.submit_text}</a></div>
                <div id='cookies-privacy'>${privacy}</div>
            </div>
        `);
    }
    $('.otCookiesNotification #cookies-wrapper').fadeIn(300);
}
function omgcookies_cookiesSubmit() {
    var getCookiesNotification = omgcookies_getCookie('cookiesNotification');
    if (getCookiesNotification == null || getCookiesNotification == '') {
        getCookiesNotification = [];
    } else {
        getCookiesNotification = JSON.parse(getCookiesNotification);
    }
    var data = {
        savedAt: new Date(),
    };
    getCookiesNotification.push(data);
    omgcookies_setCookie('cookiesNotification', getCookiesNotification);
    $('.otCookiesNotification #cookies-wrapper').fadeOut(300);
}
function ShowDetail(){
    document.getElementById("cookies-detail-hidden").style.display = "block";
    document.getElementById("cookies-detail").style.display = "none";
    document.getElementById("container-detail").style.display = "block";
}
function HiddenDetail(){
    document.getElementById("cookies-detail-hidden").style.display = "none";
    document.getElementById("cookies-detail").style.display = "block";
    document.getElementById("container-detail").style.display = "none";
}
function openType(evt, TypeName) {
    var i, x, tabLinks;
    x = document.getElementsByClassName("typeCity");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("tabLink");
    for (i = 0; i < x.length; i++) {
        tabLinks[i].className = tabLinks[i].className.replace(" omega-border-red", "");
    }
    document.getElementById(TypeName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " omega-border-red";
  }
function omgcookies_getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    console.log(keyValue);
    return keyValue ? keyValue[2] : null;
}
// function ReadCookie() {
//     var allCookies = document.cookie;
   
//     // Get all the cookies pairs in an array
//     cookieArray = allCookies.split(';');
//     // Now take key value pair out of this array
//     for(var i=0; i<cookieArray.length; i++) {
//        name = cookieArray[i].split('=')[0];
//        value = cookieArray[i].split('=')[1];
//        cookies.push(name);
//     }
//     console.log(cookies);
//  }
function omgcookies_setCookie(key, value) {
    if ((typeof value) !== 'string') {
        value = JSON.stringify(value);
    }
    var exMinutes = Number(omgcookies_storeSettings.cache_time) * 24 * 60;
    var exdate = new Date();
    exdate.setMinutes(exdate.getMinutes() + exMinutes);
    document.cookie = key + '=' + value + '; expires=' + exdate.toUTCString() + '; path=/';
}