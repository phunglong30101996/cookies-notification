let omega_cookies_notification_shopName = Shopify.shop;
let rootlinkCookiesNotification = "https://ha.omegatheme.com/cookies-notification";
let appCookiesNotificationSettings;
let omega_nameSetNotificationCookie = 'cookiesNotification';

if(appCookiesNotificationSettings.app_enable == 1){
    jQuery("body").append("<div class='otCookiesNotification'></div>");
    if(appCookiesNotificationSettings.popup_layout == 1){
        jQuery('head').append(`
            <link href='${rootlinkCookiesNotification}/assets/css/cookies-notification.css?v=${appCookiesNotificationSettings.ver_css}' rel='stylesheet' type='text/css'>
        `);
    } else {
        jQuery('head').append(`
            <link href='${rootlinkCookiesNotification}/assets/css/cookies-notification-corner.css?v=${appCookiesNotificationSettings.ver_css_corner}' rel='stylesheet' type='text/css'>
        `);
    }
    var getCookiesNotification = omega_getCookie(omega_nameSetNotificationCookie);
    if (getCookiesNotification == null || getCookiesNotification == '') {
        omega_getShopLocation(omega_getCookiesNotification);
    }
}
function omega_getShopLocation(callback) {
    if(appCookiesNotificationSettings.show_all == 1) {
        if (typeof callback == "function") {
			callback();
		}
    } else {
        if(appCookiesNotificationSettings.eu_countries != null && appCookiesNotificationSettings.eu_countries != '') {
            var eu_countries = JSON.parse(appCookiesNotificationSettings.eu_countries)
        } else {
            var eu_countries = [];
        }
        jQuery.get(rootlinkCookiesNotification+'/cookies-notification.php', {
            action: 'getShopLocation',
            shop: omega_cookies_notification_shopName,
            all_eu: appCookiesNotificationSettings.show_all_eu,
            eu_countries: eu_countries
        }, function (result) {
            if (result == 1 && typeof callback == "function") {
                callback();
            }
        });
    }
}
function omega_getCookiesNotification () {
    var privacy ="";
    if(appCookiesNotificationSettings.privacy_link != '') privacy = "<a target='_blank' id='cookies-more-info' href='"+appCookiesNotificationSettings.privacy_link+"'>"+appCookiesNotificationSettings.info_text+"</a>";
    else privacy = appCookiesNotificationSettings.info_text;
    if(appCookiesNotificationSettings.popup_layout == 1) {
        var layoutCss = "";
        if(appCookiesNotificationSettings.fullwidth_position == 1){
            layoutCss += ".otCookiesNotification #cookies-wrapper{top:0;background:"+appCookiesNotificationSettings.popup_bgcolor+";color:"+appCookiesNotificationSettings.popup_textcolor+";}";
        } else if(appCookiesNotificationSettings.fullwidth_position == 2) {
            layoutCss += ".otCookiesNotification #cookies-wrapper{bottom:0;background:"+appCookiesNotificationSettings.popup_bgcolor+";color:"+appCookiesNotificationSettings.popup_textcolor+";}";
        }
        jQuery('.otCookiesNotification').append(`
            <style>
                ${layoutCss}
                #cookies-more-info{color:${appCookiesNotificationSettings.more_textcolor};}
                #cookies-submit > a{color:${appCookiesNotificationSettings.submit_textcolor};background:${appCookiesNotificationSettings.submit_bgcolor};border-color:${appCookiesNotificationSettings.submit_textcolor};}
                ${appCookiesNotificationSettings.custom_css}
            </style>
            <div id='cookies-wrapper'>
                <div id='cookies-message'>${appCookiesNotificationSettings.message}&nbsp;${privacy}</div>
                <div id='cookies-submit'><a class='ot-cookie' onclick='omega_cookiesSubmit()'>${appCookiesNotificationSettings.submit_text}</a></div>
            </div>
        `);
    } else {
        var layoutCss = "";
        if(appCookiesNotificationSettings.corner_position == 1){
            layoutCss += ".otCookiesNotification #cookies-wrapper{top:1em;left:1em;background:"+appCookiesNotificationSettings.popup_bgcolor+";color:"+appCookiesNotificationSettings.popup_textcolor+";}";
        } else if(appCookiesNotificationSettings.corner_position == 2) {
            layoutCss += ".otCookiesNotification #cookies-wrapper{bottom:1em;left:1em;background:"+appCookiesNotificationSettings.popup_bgcolor+";color:"+appCookiesNotificationSettings.popup_textcolor+";}";
        } else if(appCookiesNotificationSettings.corner_position == 3) {
            layoutCss += ".otCookiesNotification #cookies-wrapper{top:1em;right:1em;background:"+appCookiesNotificationSettings.popup_bgcolor+";color:"+appCookiesNotificationSettings.popup_textcolor+";}";
        } else if(appCookiesNotificationSettings.corner_position == 4) {
            layoutCss += ".otCookiesNotification #cookies-wrapper{bottom:1em;right:1em;background:"+appCookiesNotificationSettings.popup_bgcolor+";color:"+appCookiesNotificationSettings.popup_textcolor+";}";
        }
        jQuery('.otCookiesNotification').append(`
            <style>
                ${layoutCss}
                #cookies-more-info{color:${appCookiesNotificationSettings.more_textcolor};}
                #cookies-submit > a{color:${appCookiesNotificationSettings.submit_textcolor};background:${appCookiesNotificationSettings.submit_bgcolor};border-color:${appCookiesNotificationSettings.submit_textcolor};}
                #cookies-privacy{color:${appCookiesNotificationSettings.submit_textcolor};}
                #cookies-privacy:hover{border-color:${appCookiesNotificationSettings.submit_textcolor};}
                ${appCookiesNotificationSettings.custom_css}
            </style>
            <div id='cookies-wrapper'>
                <div id='cookies-message'>${appCookiesNotificationSettings.message}</div>
                <div id='cookies-submit'><a class='ot-cookie' onclick='omega_cookiesSubmit()'>${appCookiesNotificationSettings.submit_text}</a></div>
                <div id='cookies-privacy'>${privacy}</div>
            </div>
        `);
    }
    jQuery('.otCookiesNotification #cookies-wrapper').fadeIn(300);
}
function omega_cookiesSubmit () {
    var getCookiesNotification = omega_getCookie('cookiesNotification');
    if(getCookiesNotification == null || getCookiesNotification == ''){
        getCookiesNotification = [];
    } else {
        getCookiesNotification = JSON.parse(getCookiesNotification);
    }
    var data = {
        savedAt: new Date(),
    };
    getCookiesNotification.push(data);
    omega_setCookie('cookiesNotification', getCookiesNotification); 
    jQuery('.otCookiesNotification #cookies-wrapper').fadeOut(300);
}
function omega_getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}
function omega_setCookie(key, value) {
    if ((typeof value) !== 'string') {
        value = JSON.stringify(value);
    }
    var exMinutes = Number(appCookiesNotificationSettings.cache_time)*24*60;
    var exdate=new Date();
    exdate.setMinutes(exdate.getMinutes() + exMinutes);
    document.cookie= key + '=' + value + '; expires='+exdate.toUTCString()+'; path=/';
}