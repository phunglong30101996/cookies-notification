<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
ini_set('display_errors', TRUE);
error_reporting(E_ALL);
require 'vendor/autoload.php';
use sandeepshetty\shopify_api;
require 'conn-shopify.php';
require 'help.php';
if (isset($_GET["action"])) {
    $action = $_GET["action"];
    $shop = $_GET["shop"];
    if ($action == "checkInstallApp") {
        $expired = checkExpired($shop, $appId, $trialTime);
        $shop_data = db_fetch_row("select * from tbl_usersettings where store_name = '$shop' and app_id = $appId");
        if(isset($shop_data['installed_date'])) {
            $installed = true;
        } else {
            $installed = false;
        }
        $response = array( 
            "install"  => $installed, 
            "expired"   => $expired,
            "ver" => 3,
        );
        echo json_encode($response);
    }
    if ($action == "getShopLocation") {
        $all_eu_countries = file_get_contents(APP_PATH."/all_eu_countries.json");
        $all_eu_countries = json_decode($all_eu_countries,true);
        $show_all_eu = isset($_GET["all_eu"]) ? $_GET["all_eu"] : 0;
        $eu_countries = isset($_GET["eu_countries"]) ? $_GET["eu_countries"] : array();
        if($show_all_eu == 1){
            $location = getShopLocation($all_eu_countries);
        } elseif($show_all_eu == 0){
            $location = getShopLocation($eu_countries);
        }
        echo json_encode($location);
    }
    if ($action == "getCookies") {
        $name = $_GET["name"];
        if ($name === '') {
            $html = '';
        } else {
            $html =   makeHtml($name);
        }

        $response = array(
            "html" => $html,
            
        );
        echo json_encode($response);
    }
}
function makeHtml($name)
{
    var_dump($_COOKIE);
    $Cookie_array = $name;
    $status = $group_arr['app_status'];
    foreach($cookieName as $Cookie_array ){
        if(!isset($_COOKIE[$cookieName])) {
            echo "Cookie named '" . $cookie_name . "' is not set!";
        } else {
            echo "Cookie '" . $cookie_name . "' is set!<br>";
            echo "Value is: " . $_COOKIE[$cookie_name].[];
        }
    }
        if ($status == 1) {
            //check working time   
            $html .= "<div></div>";  
                                  
       } else {
            $html .= "";
        }
    
    return $html;
}
function get_if() { 
    $ip = $_SERVER['REMOTE_ADDR']; 
    $userInfo = IpToInfo($ip);  
    $userInfo['ip'] = $ip;
    return $userInfo["country_code"];
}
function getShopLocation($lists) {
    $code = get_if();
    $result = 0;
	if(!empty($lists) && is_array($lists)) {
        foreach ($lists as $value){ 
            if(strpos($code,$value['value']) > -1 ){
                $result = 1;
                break;
			}
		}
	}
    return $result;
}
function checkExpired($shop, $appId, $trialTime) {
    $shop_data = db_fetch_row("select * from tbl_usersettings where store_name = '$shop' and app_id = $appId");
    if(isset($shop_data['installed_date'])) {
        $installedDate = $shop_data['installed_date'];
        $clientStatus = $shop_data['status'];
        $date1 = new DateTime($installedDate);
        $date2 = new DateTime("now");
        $interval = date_diff($date1, $date2);
        $diff = (int)$interval->format('%R%a');
        if($diff > $trialTime && $clientStatus != 'active'){
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}