<?php
/*
Restore về file JS mặc định cho 1 shop
Ex: https://apps.omegatheme.com/cookies-notification/cron/checkScriptTag.php?shop=ha-devstore.myshopify.com
*/
ini_set('display_errors', TRUE);
error_reporting(E_ALL);
require '../vendor/autoload.php';
use sandeepshetty\shopify_api;
require '../conn-shopify.php';

if (isset($_GET["shop"])) {
    $shop = $_GET["shop"];
    $shopify = shopifyInit($db, $shop, $appId);
    $delete = $shopify('DELETE', '/admin/script_tags/30392156248.json');
    $scriptTags = $shopify('GET', '/admin/script_tags.json');
    var_dump($scriptTags);
    // $settings = getShopSettings($db, $shop);
    // $putjs = $shopify('PUT', '/admin/script_tags/'.$settings["script_tagid"].'.json', array('script_tag' => array('id' => $settings["script_tagid"], 'src' => $rootLink . "/cookies-notification.js")));
    // echo $rootLink . "/cookies-notification.js"." <a href='https://".$shop."' target='_blank'>".$shop."</a>";
}

function getShopSettings($db, $shop) {
    $sql = "SELECT script_tagid FROM cookies_notification_settings WHERE shop = '$shop'";
    $query = $db->query($sql);
    $settings = array();
    if ($query) {
        while ($row = $query->fetch_assoc()) {
            $settings = $row;
        }
    }
    return $settings;
}
function shopifyInit($db, $shop, $appId) {
    $select_settings = $db->query("SELECT * FROM tbl_appsettings WHERE id = $appId");
    $app_settings = $select_settings->fetch_object();
    $shop_data = $db->query("select * from tbl_usersettings where store_name = '" . $shop . "' and app_id = $appId");
    $shop_data = $shop_data->fetch_object();
    $shopify = shopify_api\client(
        $shop, $shop_data->access_token, $app_settings->api_key, $app_settings->shared_secret
    );
    return $shopify;
}