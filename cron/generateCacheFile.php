<?php
/*
Generate JS cache file cho all shop LIMIT theo page
Ex: https://apps.omegatheme.com/cookies-notification/cron/generateCacheFile.php?page=0
*/
ini_set('display_errors', TRUE);
error_reporting(E_ALL);
require '../vendor/autoload.php';
use sandeepshetty\shopify_api;
require '../conn-shopify.php';
require '../help.php';

if (isset($_GET["page"])) {
    $page = " LIMIT 50 OFFSET ".(intval($_GET["page"])*50);
} else {
    $page = "";
}

$listShops = getListShops($db, $appId, $page);

if(is_array($listShops)) {
    for( $i = 0; $i < count($listShops); $i++ ) {
        $shopify    = shopifyInit($db, $listShops[$i]->store_name, $appId);
        createShopSettingsFile($listShops[$i]->store_name);

        $scriptTags = $shopify('GET', '/admin/script_tags.json');
        if(is_array($scriptTags) && $scriptTags[0]["id"]) {
            $putjs = $shopify('PUT', '/admin/script_tags/'.$scriptTags[0]["id"].'.json', array(
                'script_tag' => array(
                    'id' => $scriptTags[0]["id"], 
                    'src' => $rootLink . "/cookies-notification.js?v=1"
                )
            ));
        }

        echo "<br/>".($i+1).". <a href='https://".$listShops[$i]->store_name."' target='_blank'>".$listShops[$i]->store_name."</a><br/>";
    }
}

// Get shop theo page, limit từ bảng tbl_usersettings với status = 'active' và $appId
function getListShops($db, $appId, $page) {
	$query = $db->query("SELECT * FROM (
        SELECT id, store_name, installed_date, access_token FROM tbl_usersettings where status = 'active' and app_id = $appId ".$page."
      ) AS shop ORDER BY shop.installed_date desc");
	$shops = array();
	if ($query) {
		while ($row = $query->fetch_object()) {
			$shops[] = $row;
		}
	}
	return $shops;
}
function pr($data) {
    if (is_array($data)) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }else{
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
}