<?php
/*
Update Script tags id cho all shop LIMIT theo page
Ex: https://apps.omegatheme.com/cookies-notification/cron/updateScriptagId.php?page=0
*/
ini_set('display_errors', TRUE);
error_reporting(E_ALL);
require '../vendor/autoload.php';
use sandeepshetty\shopify_api;
require '../conn-shopify.php';

if (isset($_GET["page"])) {
    $page = " LIMIT 50 OFFSET ".(intval($_GET["page"])*50);
} else {
    $page = "";
}

$app_settings = appSettingsInit($db, $appId);
$listShops = getListShops($db, $appId, $page);

if(is_array($listShops)) {
    for( $i = 0; $i < count($listShops); $i++ ) {
        $shopify = shopifyInit($db, $listShops[$i], $app_settings);
        $scriptTags = $shopify('GET', '/admin/script_tags.json');
        if(is_array($scriptTags) && $scriptTags[0]["id"]) {
            $script_tagid = $scriptTags[0]["id"];
            $query = $db->query("update cookies_notification_settings set script_tagid = '$script_tagid' where shop = '" . $listShops[$i]->store_name . "'");
            if($query) {
                echo "<br/>".($i+1).". Inserted script tags id for ".$listShops[$i]->store_name.": Done!<br/>";
            }
        } else {
            echo "<br/>".($i+1).". Inserted script tags id for ".$listShops[$i]->store_name.": Fail!<br/>";
        }
    }
}

function appSettingsInit($db, $appId) {
    $select_settings = $db->query("SELECT * FROM tbl_appsettings WHERE id = $appId");
    $app_settings = $select_settings->fetch_object();
    return $app_settings;
}
function shopifyInit($db, $shop_data, $app_settings) {
    $shopify = shopify_api\client(
        $shop_data->store_name, $shop_data->access_token, $app_settings->api_key, $app_settings->shared_secret
    );
    return $shopify;
}
function getListShops($db, $appId, $page) {
	$query = $db->query("SELECT * FROM (
        SELECT id, store_name, installed_date, access_token FROM tbl_usersettings where status = 'active' and app_id = $appId ".$page."
      ) AS shop ORDER BY shop.installed_date desc");
	$shops = array();
	if ($query) {
		while ($row = $query->fetch_object()) {
			$shops[] = $row;
		}
	}
	return $shops;
}

function pr($data) {
    if (is_array($data)) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }else{
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
}