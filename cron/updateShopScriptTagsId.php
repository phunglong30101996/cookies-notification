<?php
/*
Update Script tags id cho 1 shop
Ex: https://apps.omegatheme.com/cookies-notification/cron/updateShopScriptTagsId.php?shop=ha-devstore.myshopify.com
*/
ini_set('display_errors', TRUE);
error_reporting(E_ALL);
require '../vendor/autoload.php';
use sandeepshetty\shopify_api;
require '../conn-shopify.php';

if (isset($_GET["shop"])) {
    $shop = $_GET["shop"];
    $shopify = shopifyInit($db, $shop, $appId);
    $scriptTags = $shopify('GET', '/admin/script_tags.json');
    if(is_array($scriptTags) && $scriptTags[0]["id"]) {
        $script_tagid = $scriptTags[0]["id"];
        $query = $db->query("update cookies_notification_settings set script_tagid = '$script_tagid' where shop = '$shop'");
        if($query) {
            echo "<br/>Inserted script tags id for ".$shop.": Done!<br/>";
        }
    } else {
        echo "<br/>Inserted script tags id for ".$shop.": Fail!<br/>";
    }
}
function shopifyInit($db, $shop, $appId) {
    $select_settings = $db->query("SELECT * FROM tbl_appsettings WHERE id = $appId");
    $app_settings = $select_settings->fetch_object();
    $shop_data = $db->query("select * from tbl_usersettings where store_name = '" . $shop . "' and app_id = $appId");
    $shop_data = $shop_data->fetch_object();
    $shopify = shopify_api\client(
        $shop, $shop_data->access_token, $app_settings->api_key, $app_settings->shared_secret
    );
    return $shopify;
}
function pr($data) {
    if (is_array($data)) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }else{
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
}