<?php
ini_set('display_errors', TRUE);
error_reporting(E_ALL); 
require 'vendor/autoload.php';
use sandeepshetty\shopify_api;
require 'conn-shopify.php';
require 'help.php';
if (isset($_GET["action"])) {
    $action = $_GET["action"];
    $shop = $_GET["shop"];

    $shopify = shopifyInit($db, $shop, $appId);
    if ($action == "getSettings") {
        $settings = getShopSettings($db, $shop);
        echo json_encode($settings);
    }
    if ($action == "getStorePlan") {
        $webhookContent = "";
        $webhook = fopen('php://input' , 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        fclose($webhook);
        $data = json_decode($webhookContent, true);
        if($data["plan_name"] == 'closed' || $data["plan_name"] == 'cancelled' || $data["plan_name"] == 'fraudulent') {
            $db->query("update tbl_usersettings set status = '".$data["plan_name"]."' where shop = '$shop' and app_id = $appId");
        } else {
            $db->query("update tbl_usersettings set status = 'active' where shop = '$shop' and app_id = $appId");
        }
        
    }
}
if (isset($_POST['action'])) {
    $action     = $_POST['action'];
    $shop       = $_POST["shop"];
    if ($action == "saveSettings") {
        $shopify = shopifyInit($db, $shop, $appId);
        $settings = $_POST["settings"];
        $settings['eu_countries'] = (isset($settings['eu_countries']) && is_array($settings['eu_countries']))?json_encode($settings['eu_countries'], JSON_UNESCAPED_UNICODE):"[]";

        $data = array(
            'app_enable'            => returnBooleanData($settings["app_enable"]),
            'message'               => $settings["message"],
            'submit_text'           => $settings["submit_text"],
            'info_text'             => $settings["info_text"],
            'privacy_link'          => $settings["privacy_link"],
            'popup_layout'          => $settings["popup_layout"],
            'fullwidth_position'    => $settings["fullwidth_position"],
            'corner_position'       => $settings["corner_position"],
            'show_all'              => returnBooleanData($settings["show_all"]),
            'show_all_eu'           => returnBooleanData($settings["show_all_eu"]),
            'eu_countries'          => $settings["eu_countries"],
            'cache_time'            => $settings["cache_time"],
            'popup_bgcolor'         => $settings["popup_bgcolor"],
            'popup_textcolor'       => $settings["popup_textcolor"],
            'more_textcolor'        => $settings["more_textcolor"],
            'submit_bgcolor'        => $settings["submit_bgcolor"],
            'submit_textcolor'      => $settings["submit_textcolor"],
            'custom_css'            => $settings["custom_css"],
        );  
        db_update("cookies_notification_settings",$data,"shop = '$shop'");
        createShopSettingsFile($shop);
        saveScriptTagId($shop, $shopify, 'cookies_notification_settings');
        updateScriptTag($shop, $shopify, 'cookies_notification_settings', $rootLink.'/cookies-notification.js');
    }
}

function getShopSettings($db, $shop) {
    $sql = "SELECT * FROM cookies_notification_settings WHERE shop = '$shop' ORDER BY id DESC";
    $query = $db->query($sql);
    $settings = array();
    if ($query) {
        while ($row = $query->fetch_assoc()) {
            $row["eu_countries"] = json_decode($row["eu_countries"]);
            if(!is_array($row["eu_countries"])) {
                $row["eu_countries"] = array();
            }
            $settings = $row;
        }
    }
    return $settings;
}