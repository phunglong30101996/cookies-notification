<?php

require 'conn-shopify.php';
require 'help.php';
session_start();

unset($_SESSION['shop']);
$webhookContent = "";

$webhook = fopen('php://input', 'rb');
while (!feof($webhook)) {
    $webhookContent .= fread($webhook, 4096);
}

fclose($webhook);
$webhookContent = json_decode($webhookContent);
if (isset($webhookContent->myshopify_domain)) {
    $shop = $webhookContent->myshopify_domain;
    db_delete("tbl_usersettings",'store_name = "' . $shop . '" and app_id = ' . $appId);
    db_delete("cookies_notification_settings","shop = '" . $shop . "'");
    deleteDataCache(CACHE_PATH . $shop);
	// Gui email cho customer khi uninstalled
	require 'email/uninstall_email.php';	
} else if (isset($webhookContent->domain)) {
    $shop = $webhookContent->domain;
    db_delete("tbl_usersettings",'store_name = "' . $shop . '" and app_id = ' . $appId);
    db_delete("cookies_notification_settings","shop = '" . $shop . "'");
    deleteDataCache(CACHE_PATH . $shop);
	// Gui email cho customer khi uninstalled
	require 'email/uninstall_email.php';	
}


